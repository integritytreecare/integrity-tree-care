Professional tree service located in Indian Trail, NC and serving the complete Charlotte, NC area. We are fully licensed, insured and have over 20 years experience in the industry. Offering tree removal, tree trimming, stump removal and more.

Address: 5304 Carol Ave, Indian Trail, NC 28079, USA

Phone: 704-608-7285

Website: https://integritytreepros.com/
